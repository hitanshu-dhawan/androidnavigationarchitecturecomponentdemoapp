package com.hitanshudhawan.navigationexample

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import kotlinx.android.synthetic.main.fragment_question_2.*

class Question2Fragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_question_2, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val questionArgs = Question1FragmentArgs.fromBundle(arguments!!)
        fragment_question_category_key_text_view.text = questionArgs.categoryKey

        fragment_question_next_button.setOnClickListener {
            // some logic
            findNavController().navigate(R.id.question_3_dest, arguments)
        }
    }

}