package com.hitanshudhawan.navigationexample

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import kotlinx.android.synthetic.main.fragment_payments.*

class PaymentsFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_payments, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val paymentsArgs = PaymentsFragmentArgs.fromBundle(arguments!!)
        fragment_payments_category_key_text_view.text = paymentsArgs.categoryKey

        fragment_payments_next_button.setOnClickListener {
            val action = PaymentsFragmentDirections.actionNext()
            findNavController().navigate(action)
            Toast.makeText(context, "Request Placed", Toast.LENGTH_SHORT).show()
        }
    }

}