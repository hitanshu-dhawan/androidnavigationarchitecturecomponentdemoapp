package com.hitanshudhawan.navigationexample

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import kotlinx.android.synthetic.main.fragment_luminosity.*

class LuminosityFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_luminosity, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val luminosityArgs = LuminosityFragmentArgs.fromBundle(arguments!!)
        fragment_luminosity_category_key_text_view.text = luminosityArgs.categoryKey

        fragment_luminosity_try_men_grooming_button.setOnClickListener {
            val action = LuminosityFragmentDirections.actionGlobalLuminosityDest("men_grooming")
            findNavController().navigate(action)
        }

        fragment_luminosity_next_button.setOnClickListener {
            val action = LuminosityFragmentDirections.actionNext(luminosityArgs.categoryKey)
            findNavController().navigate(action)
        }
    }

}