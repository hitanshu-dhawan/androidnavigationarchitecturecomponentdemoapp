package com.hitanshudhawan.navigationexample

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import kotlinx.android.synthetic.main.fragment_search.*

class SearchFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_search, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // https://stackoverflow.com/a/51500083
        fragment_search_salon_at_home_button.setOnClickListener {
            val action = MainNavigationDirections.actionGlobalLuminosityDest("salon_at_home")
            Navigation.findNavController(activity!!, R.id.main_nav_host_fragment).navigate(action)
        }
        fragment_search_massage_for_men_button.setOnClickListener {
            val action = MainNavigationDirections.actionGlobalLuminosityDest("massage_for_men")
            Navigation.findNavController(activity!!, R.id.main_nav_host_fragment).navigate(action)
        }
        fragment_search_ac_servicing_button.setOnClickListener {
            val action = MainNavigationDirections.actionGlobalLuminosityDest("ac_servicing")
            Navigation.findNavController(activity!!, R.id.main_nav_host_fragment).navigate(action)
        }
        fragment_search_packers_and_movers_button.setOnClickListener {
            val action = MainNavigationDirections.actionGlobalLuminosityDest("packers_and_movers")
            Navigation.findNavController(activity!!, R.id.main_nav_host_fragment).navigate(action)
        }
    }

}