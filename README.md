# Navigation Architecture Component

![picture](images/Screenshot1.png)

![picture](images/Screenshot2.png)

## Resources / Links

https://developer.android.com/topic/libraries/architecture/navigation

https://codelabs.developers.google.com/codelabs/android-navigation/#0

https://developer.android.com/topic/libraries/architecture/navigation/navigation-implementing

https://youtu.be/ELGShpd17wc

https://stackoverflow.com/a/51500083

https://developer.android.com/guide/navigation/navigation-nested-graphs

https://developer.android.com/guide/navigation/navigation-global-action

https://developer.android.com/guide/navigation/navigation-pass-data#safe-args-global

https://youtu.be/2k8x8V77CrU

https://medium.com/a-problem-like-maria/a-problem-like-navigation-e9821625a70e

https://medium.com/a-problem-like-maria/a-problem-like-navigation-part-2-63e46a565d4b

https://proandroiddev.com/why-i-will-not-use-architecture-navigation-component-97d2ad596b36